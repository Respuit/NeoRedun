My first "real" program.

It will make an exact copy of a selected directory and paste it in the desired one.
Note that it will only paste or add a file if the last modification of said file is later than the one existing on the backup folder or if it doesn't exist.